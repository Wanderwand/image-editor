let file;
let originalWidth, originalHeight;

const addFile = document.querySelector("#addFile");
const inputFile = document.querySelector("#inputFile");
const uploaded = document.querySelector("#uploaded");
const previewImg = document.querySelector("#preview__image");
const originalImg = document.querySelector("#original__image");
// function to read and store filename and dimensions
const readFile = (file) => {
  const reader = new FileReader();
  reader.addEventListener("load", () => {
    previewImg.src = reader.result;
    originalImg.src = reader.result;
    uploaded.classList.remove("hidden");
    // get the original dimensions of the image
    var img = new Image();
    img.onload = function () {
      originalHeight = img.height;
      originalWidth = img.width;
    };
    img.src = reader.result;
  });
  if (file) {
    reader.readAsDataURL(file);
    addFile.classList.add("hidden");
  }

  document.querySelector("#overlay").style.width = previewImg.style.width ;
  document.querySelector("#overlay").style.height = previewImg.style.height ;

};

inputFile.addEventListener("change", (e) => {
  file = e.target.files[0];
  readFile(file);
});

const dropable = document.querySelector("#dropable");

// hide the alerts
function hideAlert(e) {
  const alertObj = e.target.parentElement;
  alertObj.classList.add("hidden");
}

// drop handler
dropable.addEventListener("drop", (e) => {
  e.preventDefault();
  file = e.target.files[0];
  readFile(file);
});

function cancel(e) {
  // going back to home page
  location.reload();
}

function clearCanvas() {
  const canvas = document.getElementById("canvas");
  const context = canvas.getContext("2d");
  context.clearRect(0, 0, canvas.width, canvas.height);
}

function showResize(event) {
  clearCanvas();
  document.querySelector("#isResize").classList.remove("hidden");
  document.querySelector("#showResize").classList.add("border-b-2");

  document.querySelector("#isCrop").classList.add("hidden");
  document.querySelector("#showCrop").classList.remove("border-b-2");
  document.querySelector("#overlay").classList.add("hidden");

  document.querySelector("#isOutput").classList.remove("hidden");
}

function showCrop(event) {
  clearCanvas();

  document.querySelector("#isCrop").classList.remove("hidden");
  document.querySelector("#showCrop").classList.add("border-b-2");
  document.querySelector("#overlay").classList.remove("hidden");

  document.querySelector("#isResize").classList.add("hidden");
  document.querySelector("#showResize").classList.remove("border-b-2");

  document.querySelector("#isOutput").classList.remove("hidden");

  document.querySelector("#preview").scrollIntoView({behavior: "smooth", block: "start"});

}

function dataTable(type) {
  let width=document.querySelector("canvas").width
  let height=document.querySelector("canvas").height
  //Calculating and Assigning sizes and dimensions
  const originalSize =
    file.size > 1024
      ? file.size > 1048576
        ? Math.round(file.size / 1048576) + " mb"
        : Math.round(file.size / 1024) + " kb"
      : file.size + " b";

  // calculating the size of canvas
  let newSize;
  canvas.toBlob(
    (blob) => {
      newSize = blob.size;
      const size =
        newSize > 1024
          ? newSize > 1048576
            ? Math.round(newSize / 1048576) + " mb"
            : Math.round(newSize / 1024) + " kb"
          : newSize + " b";
      document
        .querySelectorAll("#newSize")
        .forEach((el) => (el.innerHTML = size));
    },
    "image/png",
    1
  );

  document.querySelectorAll("#fileName").forEach((el) => {
    el.innerHTML = `${type}.png`;
  });
  document
    .querySelectorAll("#originalSize")
    .forEach((el) => (el.innerHTML = originalSize));
  document
    .querySelectorAll("#originalDimensions")
    .forEach((el) => (el.innerHTML = `${originalWidth} x ${originalHeight}`));
  document
    .querySelectorAll("#dimensions")
    .forEach((el) => (el.innerHTML = `${width} x ${height}`));
  document.querySelector("#outputCanvas").classList.remove("hidden");
  document.querySelector("#downloadDiv").classList.remove("hidden");
  document.querySelector("#outputHeading").scrollIntoView({behavior: "smooth"});
}

function download(type) {
  const width = parseInt(document.querySelector("#width__input").value);
  const height = parseInt(document.querySelector("#height__input").value);
  const download = document.getElementById("download");
  const image = document
    .getElementById("canvas")
    .toDataURL("image/png", 1)
    .replace("image/png", "image/octet-stream");
  if (type === "resized") {
    download.download = `${width}x${height}-resized.png`;
  } else {
    download.download = `cropped-imgage.png`;
  }
  download.setAttribute("href", image);
}
