function resize(e){
  const  canvas = document.querySelector("#canvas");
  let width=document.querySelector("#width__input").value
  let height=document.querySelector("#height__input").value
  const inValid =document.querySelector("#inValid")
  const empty=document.querySelector("#empty")
  // check whether dimensions entred are empty  
  if(width ==="" || height==="" || width == "0" || height ==="0"){
    empty.classList.remove("hidden")
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    setTimeout(()=>empty.classList.add("hidden"), 3000);
    return 
  }
  // check whether dimensions entred are correct 
  let regex = /^\d+$/;
  if(!regex.test(width) || !regex.test(height) ){
    inValid.classList.remove("hidden")
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    setTimeout(() => inValid.classList.add("hidden"),3000);
    return 
  }
  // convert dimensions to number from string 
  width = parseInt(width);
  height = parseInt(height)
  // drawing canvas  
  let ctx = canvas.getContext("2d");
  canvas.width=width;
  canvas.height=height;
  // resizing using opencv
  let src = cv.imread('original__image');
  let dst = new cv.Mat();
  let dsize = new cv.Size(width, height);
  cv.resize(src, dst, dsize, 0, 0, cv.INTER_AREA);
  cv.imshow('canvas', dst);
  src.delete(); dst.delete();

  dataTable("resized")

}

// download in png format 
download("resized")
