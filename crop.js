// function getCordinates (event){
//   var x = event.clientX;
//   var y = event.clientY;
//   var coords = "X coords: " + x + ", Y coords: " + y;
//   console.log(coords)
// }

var container = document.querySelector("#preview__imageDiv");
const ptFixed = document.querySelector("#ptFixed");

function coordinatesOfPts() {
  let coordinateObj = ptFixed.getBoundingClientRect();
  // console.log(coordinateObj )
  let X = coordinateObj.x;
  let Y = coordinateObj.y;
  const ptOne = document.querySelector("#one").getBoundingClientRect();
  const ptTwo = document.querySelector("#two").getBoundingClientRect();
  const ptThree = document.querySelector("#three").getBoundingClientRect();
  const ptFour = document.querySelector("#four").getBoundingClientRect();
  const ptsArr = [
    ptOne.x - X,
    ptOne.y - Y,
    ptTwo.x - X,
    ptTwo.y - Y,
    ptThree.x - X,
    ptThree.y - Y,
    ptFour.x - X,
    ptFour.y - Y,
  ];
  return ptsArr;
}
const ptsArr = coordinatesOfPts();
document
  .querySelector("#polygonSvg")
  .setAttribute(
    "points",
    `${ptsArr[0]},${ptsArr[1]},${ptsArr[2]},${ptsArr[3]},${ptsArr[6]},${ptsArr[7]},${ptsArr[4]},${ptsArr[5]}`
  );

var activeItem = null;
var active = false;

container.addEventListener("touchstart", dragStart, false);
container.addEventListener("touchend", dragEnd, false);
container.addEventListener("touchmove", drag, false);

container.addEventListener("mousedown", dragStart, false);
container.addEventListener("mouseup", dragEnd, false);
container.addEventListener("mousemove", drag, false);

function dragStart(e) {
  if (
    e.target.id === "preview__image" ||
    e.target.id === "polygonSvg" ||
    e.target.id === "svg"
  ) {
    console.log("image");
    return;
  }

  if (e.target !== e.currentTarget) {
    active = true;

    // this is the item we are interacting with
    activeItem = e.target;

    if (activeItem !== null) {
      if (!activeItem.xOffset) {
        activeItem.xOffset = 0;
      }

      if (!activeItem.yOffset) {
        activeItem.yOffset = 0;
      }

      if (e.type === "touchstart") {
        activeItem.initialX = e.touches[0].clientX - activeItem.xOffset;
        activeItem.initialY = e.touches[0].clientY - activeItem.yOffset;
      } else {
        activeItem.initialX = e.clientX - activeItem.xOffset;
        activeItem.initialY = e.clientY - activeItem.yOffset;
      }
    }
  }
}

function dragEnd(e) {
  if (activeItem !== null) {
    activeItem.initialX = activeItem.currentX;
    activeItem.initialY = activeItem.currentY;
  }

  active = false;
  activeItem = null;
}

function drag(e) {
  if (active) {
    if (e.type === "touchmove") {
      e.preventDefault();

      activeItem.currentX = e.touches[0].clientX - activeItem.initialX;
      activeItem.currentY = e.touches[0].clientY - activeItem.initialY;
    } else {
      activeItem.currentX = e.clientX - activeItem.initialX;
      activeItem.currentY = e.clientY - activeItem.initialY;
    }

    activeItem.xOffset = activeItem.currentX;
    activeItem.yOffset = activeItem.currentY;

    setTranslate(activeItem.currentX, activeItem.currentY, activeItem);
  }
}

function setTranslate(xPos, yPos, el) {
  el.style.transform = "translate3d(" + xPos + "px, " + yPos + "px, 0)";
  const ptsArr = coordinatesOfPts();
  document
    .querySelector("#polygonSvg")
    .setAttribute(
      "points",
      `${ptsArr[0]},${ptsArr[1]},${ptsArr[2]},${ptsArr[3]},${ptsArr[6]},${ptsArr[7]},${ptsArr[4]},${ptsArr[5]}`
    );
}

function crop(e) {
  const previewImg = document.querySelector("#preview__image");
  let coordinateObj = ptFixed.getBoundingClientRect();
  // console.log(coordinateObj )
  let X = coordinateObj.x;
  let Y = coordinateObj.y;
  const ptOne = document.querySelector("#one").getBoundingClientRect();
  const ptTwo = document.querySelector("#two").getBoundingClientRect();
  const ptThree = document.querySelector("#three").getBoundingClientRect();
  const ptFour = document.querySelector("#four").getBoundingClientRect();

  // console.log(ptOne , ptTwo , ptThree , ptFour)
  const ptsArr = [
    ptOne.x - X,
    ptOne.y - Y,
    ptTwo.x - X,
    ptTwo.y - Y,
    ptThree.x - X,
    ptThree.y - Y,
    ptFour.x - X,
    ptFour.y - Y,
  ];

  const maxWidth = Math.max(ptsArr[2] - ptsArr[0], ptsArr[6] - ptsArr[4]);
  const maxHeight = Math.max(ptsArr[5] - ptsArr[1], ptsArr[7] - ptsArr[3]);
  // onpen cv
  let src = cv.imread("preview__image");
  let dst = new cv.Mat();
  let dsize = new cv.Size(maxWidth, maxHeight);
  // codinates of the image to be cropped
  let srcTri = cv.matFromArray(4, 1, cv.CV_32FC2, [...ptsArr]);
  //
  let dstTri = cv.matFromArray(4, 1, cv.CV_32FC2, [
    0,
    0,
    maxWidth,
    0,
    0,
    maxHeight,
    maxWidth,
    maxHeight,
  ]);
  let M = cv.getPerspectiveTransform(srcTri, dstTri);
  // You can try more different parameters
  cv.warpPerspective(
    src,
    dst,
    M,
    dsize,
    cv.INTER_LINEAR,
    cv.BORDER_CONSTANT,
    new cv.Scalar()
  );
  cv.imshow("canvas", dst);
  src.delete();
  dst.delete();
  M.delete();
  srcTri.delete();
  dstTri.delete();

  document.querySelector("#canvas").classList.remove("hidden");

  document.querySelector("#outputCanvas").classList.remove("hidden");
  document.querySelector("#downloadDiv").classList.remove("hidden");

  dataTable("cropped");
}

download("cropped");
